# **Java Base to Monster** ![Black panther](/imagens/favicon.png)

## Guia inicial, rápido, para criar um projeto Spring Boot



#### Preparação do Ambiente de Desenvolvimento

Para começar, Você precisar ter

- JDK 11 (Java Development Kit)
- IntelliJ IDEA CE (Community Edition) ou Eclipse



## Etapa 1: iniciar um novo projeto Spring Boot

Usar https://start.spring.io/ para criar um projeto “web”. Na caixa de diálogo “Dependencies”, procure e adicione a dependência “web” conforme mostrado na captura de tela. Clique no botão “Gerar”, baixe o zip e descompacte-o em uma pasta no seu computador.

![Tela do spring.io](/imagens/passo1.jpg)

- No campo Group você precisa colocar o nome do pacote principal, que seguindo as recomendações precisar ser o endereço do site de trás pra frente exemplo com.exemplo

- Artifect e Name você colca o nome do projeto, por exemplo demon

- Versão do java tem que ser a 11



Depois de descompactado o projeto você abre ele na IDE que você preferir(Nesse exemplo vamos usar o IntelliJ)

![tela da ide com o projeto](/imagens/abrindoProjeto.jpg)



O arquivo pom XML é onde vai ficar todas as configurações de gerênciamento de dependências e do projeto, para fazer isso estamos utilizando o MAVEN.

Na imagem abaixo segue um exemplo de um arquivo pom.xml

![pom xml](/imagens/pom-xml.jpg)


Para rodar o projeto basta abrir a classe principal e rodar

![iniciando projeto](/imagens/rodandoProjeto.jpg)



### OBS: Seguir todos os passos feito no curso
