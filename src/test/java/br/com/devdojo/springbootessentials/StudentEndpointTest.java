package br.com.devdojo.springbootessentials;

import br.com.devdojo.springbootessentials.awesome.SpringbootEssentialsApplication;
import br.com.devdojo.springbootessentials.awesome.model.Student;
import br.com.devdojo.springbootessentials.awesome.repository.StudentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;


import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;




@ContextConfiguration(classes = SpringbootEssentialsApplication.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class StudentEndpointTest {
    @Autowired
    private TestRestTemplate restTemplate;
    @LocalServerPort
    private int port;
    @MockBean
    private StudentRepository studentRepository;
    @Autowired
    private MockMvc mockMvc;

    @TestConfiguration
    static class Config{
        @Bean
        public RestTemplateBuilder restTemplateBuilder(){
            return new RestTemplateBuilder().basicAuthentication("bomfim", "bomfim");
        }
    }

    @Test
    public void listStudentsWhenUsernameAndPasswordAreIncorrectShouldReturnStatusCode401(){
        System.out.println(port);
        restTemplate = restTemplate.withBasicAuth("1", "1");
        ResponseEntity<String> response = restTemplate.getForEntity("/v1/protected/students/", String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    public void getStudentsWhenUsernameAndPasswordAreIncorrectShouldReturnStatusCode401(){
        System.out.println(port);
        restTemplate = restTemplate.withBasicAuth("1", "1");
        ResponseEntity<String> response = restTemplate.getForEntity("/v1/protected/students/2", String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(401);
    }

    @Test
    @WithMockUser(username = "bomfim", password = "bomfim", roles = "USER")
    public void listStudentsWhenUsernameAndPasswordAreCorrectShouldReturnStatusCode200() throws Exception {
        List<Student> students = asList(new Student(1L, "legolas", "legolas@email"),
                new Student(2L, "aragorn", "aragorn@email.com"));
        Page<Student> studentsPage = new PageImpl(students);

        BDDMockito.when(studentRepository.findAll(isA(Pageable.class))).thenReturn(studentsPage);
        MockHttpServletResponse response = mockMvc.perform(get("/v1/protected/students")).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    @WithMockUser(username = "bomfim", password = "bomfim", roles = "USER")
    public void getStudentsWhenUsernameAndPasswordAreCorrectShouldReturnStatusCode200() throws Exception {
        Student student = new Student(1L, "legolas", "legolas@email");
        BDDMockito.when(studentRepository.findById(student.getId())).thenReturn(java.util.Optional.of(student));
        MockHttpServletResponse response = mockMvc.perform(get("/v1/protected/students/{id}", student.getId())).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    @WithMockUser(username = "bomfim", password = "bomfim", roles = "USER")
    public void getStudentsWhenUsernameAndPasswordAreCorrectAndStudentDoesNotExistShouldReturnStatusCode404() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/v1/students/{id}", -1)).andReturn().getResponse();
        assertThat(response.getStatus()).isEqualTo(404);
    }

    @Test
    @WithMockUser(username = "bomfim", password = "bomfim", roles = "ADMIN")
    public void deleteWhenUserHasRoleAdminAndStudentExistsShouldReturnStatusCode200() throws Exception {
        Student student = new Student(1L, "legolas", "legolas@hotmail.com");
        BDDMockito.when(studentRepository.findById(student.getId())).thenReturn(java.util.Optional.of(student));
        BDDMockito.doNothing().when(studentRepository).deleteById(student.getId());

        mockMvc.perform(delete("/v1/admin/students/{id}", student.getId())).andExpect(status().isOk()).andDo(print());

    }

    @Test
    @WithMockUser(username = "irineu", password = "bomfim", roles = "ADMIN")
    public void deleteWhenUserHasRoleAdminAndStudentDoesNotExistsShouldReturnStatusCode404() throws Exception {
        Student student = new Student(1L, "legolas", "legolas@hotmail.com");
        BDDMockito.when(studentRepository.findById(student.getId())).thenReturn(java.util.Optional.of(student));
        BDDMockito.doNothing().when(studentRepository).deleteById(student.getId());

        mockMvc.perform(delete("/v1/students/{id}", student.getId() + 1)).andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(username = "bomfim", password = "bomfim", roles = "USER")
    public void deleteWhenUserDoesNotHaveRoleAdminShouldReturnsStatusCode403() throws Exception{
        BDDMockito.doNothing().when(studentRepository).deleteById(1L);
        mockMvc.perform(delete("/v1/admin/students/{id}", 1))
                .andExpect(status().isForbidden()).andDo(print());
    }

    @Test
    @WithMockUser(username = "bomfim", password = "bomfim", roles = "ADMIN")
    public void createWhenNameIdNullShouldReturnStatusCode400BadRequest() throws Exception{
        Student student = new Student(1L, null, "irineu@hotmail.com");
        BDDMockito.when(studentRepository.save(student)).thenReturn(student);

        mockMvc.perform(post("/v1/admin/students/", student))
                .andExpect(status().isBadRequest()).andDo(print());
    }

    @Test
    @WithMockUser(username = "bomfim", password = "bomfim", roles = "ADMIN")
    public void createShouldPersisData() throws Exception{
        Student student = new Student(5L, "legolas", "legolas@hotmail.com");

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String studentJson = ow.writeValueAsString(student);

        BDDMockito.when(studentRepository.save(student)).thenReturn(student);
        mockMvc.perform(post("/v1/admin/students/").contentType(MediaType.APPLICATION_JSON).content(studentJson))
                .andExpect(status().isCreated())
                .andDo(print());

    }
}

