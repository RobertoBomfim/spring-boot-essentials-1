package br.com.devdojo.springbootessentials;

import br.com.devdojo.springbootessentials.awesome.SpringbootEssentialsApplication;
import br.com.devdojo.springbootessentials.awesome.model.Student;
import br.com.devdojo.springbootessentials.awesome.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ContextConfiguration(classes = SpringbootEssentialsApplication.class)
@ExtendWith(SpringExtension.class)
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class StudentRepositoryTest {
    @Autowired
    private StudentRepository studentRepository;

    @PersistenceContext
    private EntityManager entityManager;

    @Test
    public void whenCreate_thenPersistData() {
        Student student = new Student("Roberto", "roberto@email.com");
        this.studentRepository.save(student);

        assertThat(student.getId()).isNotNull();
        assertThat(student.getName()).isEqualTo("Roberto");
        assertThat(student.getEmail()).isEqualTo("roberto@email.com");
    }

    @Test
    public void whenDelete_thenRemoveData(){
        Student student = new Student("Roberto", "roberto@email.com");

        this.studentRepository.save(student);
        System.out.println(studentRepository.findById(student.getId()));
        this.studentRepository.delete(student);
        System.out.println(studentRepository.findById(student.getId()));

        assertThat(studentRepository.findById(student.getId())).isEmpty();
    }

    @Test
    public void whenUpdate_thenChangeAndPersistData(){
        Student student = new Student("Roberto", "roberto@email");
        this.studentRepository.save(student);
        student.setName("Bomfim");
        student.setEmail("bomfim@email.com");
        this.studentRepository.save(student);

        student = studentRepository.findById(student.getId()).orElse(null);
        System.out.println(student);

        assertThat(student.getId()).isNotNull();
        assertThat(student.getName()).isEqualTo("Bomfim");
        assertThat(student.getEmail()).isEqualTo("bomfim@email.com");
    }

    @Test
    public void whenFindByNameIgnoreCaseContaining_thenIgnoreCase(){
        Student student = new Student("Roberto", "roberto@email.com");
        Student student2 = new Student("roberto", "robertobomfim@email.com");
        this.studentRepository.save(student);
        this.studentRepository.save(student2);

        List<Student> studentList = studentRepository.findByNameIgnoreCaseContaining("roberto");
        for (Student s :
                studentList) {
            System.out.println(s);
        }
        assertThat(studentList.size()).isEqualTo(2);
    }

    @Test
    public void whenNameIsEmpty_thenThrowConstraintViolationException(){

        Exception exception = assertThrows(
                ConstraintViolationException.class,
                () -> {
                    studentRepository.save(new Student("", "bomfim@email.com"));
                    entityManager.flush();
                });

        assertTrue(exception.getMessage().contains("O campo nome do estudante é obrigatorio"));

    }

    @Test
    public void whenEmailIsEmpty_thenThrowConstraintViolationException(){
        Exception exception = assertThrows(
                ConstraintViolationException.class,
                () -> {
                    studentRepository.save(new Student("Roberto", ""));
                    entityManager.flush();
                });
        assertTrue(exception.getMessage().contains("O campo email é obrigatório"));
    }

    @Test
    public void whenEmailIsNotValid_thenThrowConstraintViolationException(){
        Exception exception = assertThrows(
                ConstraintViolationException.class,
                () -> {
                    studentRepository.save(new Student("Roberto", "bomfim"));
                    entityManager.flush();
                });
        assertTrue(exception.getMessage().contains("O email deve ser válido"));
    }
}

