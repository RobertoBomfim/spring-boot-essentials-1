package br.com.devdojo.springbootessentials.awesome.repository;

import br.com.devdojo.springbootessentials.awesome.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    User findByUsername(String username);
}
